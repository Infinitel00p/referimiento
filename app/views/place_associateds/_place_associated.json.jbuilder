json.extract! place_associated, :id, :place_id, :associated_id, :created_at, :updated_at
json.url place_associated_url(place_associated, format: :json)
