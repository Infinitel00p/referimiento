json.extract! referral, :id, :date, :token, :used, :created_at, :updated_at
json.url referral_url(referral, format: :json)
