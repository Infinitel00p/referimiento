json.extract! associated_place_referral, :id, :place_associated_id, :referral_id, :created_at, :updated_at
json.url associated_place_referral_url(associated_place_referral, format: :json)
