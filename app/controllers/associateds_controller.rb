class AssociatedsController < ApplicationController
  load_and_authorize_resource
  before_action :set_associated, only: [:show, :edit, :update, :destroy]

  # GET /associateds
  # GET /associateds.json
  def index
    @associateds = Associated.all
  end

  # GET /associateds/1
  # GET /associateds/1.json
  def show
  end

  # GET /associateds/new
  def new
    @associated = Associated.new
  end

  # GET /associateds/1/edit
  def edit
  end

  # POST /associateds
  # POST /associateds.json
  def create
    @associated = Associated.new(associated_params)

    respond_to do |format|
      if @associated.save
        format.html { redirect_to @associated, notice: 'Asociado fue creado con éxito.' }
        format.json { render :show, status: :created, location: @associated }
      else
        format.html { render :new }
        format.json { render json: @associated.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /associateds/1
  # PATCH/PUT /associateds/1.json
  def update
    respond_to do |format|
      if @associated.update(associated_params)
        format.html { redirect_to @associated, notice: 'Asociado se actualizó correctamente.' }
        format.json { render :show, status: :ok, location: @associated }
      else
        format.html { render :edit }
        format.json { render json: @associated.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /associateds/1
  # DELETE /associateds/1.json
  def destroy
    @associated.destroy
    respond_to do |format|
      format.html { redirect_to associateds_url, notice: 'Asociado fue destruido con éxito.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_associated
      @associated = Associated.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def associated_params
      params.require(:associated).permit(:names, :surnames, :id, :no_casa, :address, :user_id)
    end
end
