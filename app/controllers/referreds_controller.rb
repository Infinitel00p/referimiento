class ReferredsController < ApplicationController
  load_and_authorize_resource 
  before_action :set_referred, only: [:show, :edit, :update, :destroy]

  # GET /referreds
  # GET /referreds.json
  def index
    @referreds = Referred.get_by_associated current_user.associated
  end

  # GET /referreds/1
  # GET /referreds/1.json
  def show
  end

  # GET /referreds/new
  def new
    @referred = Referred.new
  end

  # GET /referreds/1/edit
  def edit
  end

  # POST /referreds
  # POST /referreds.json
  def create
    @referred = Referred.new(referred_params)
    @referred.associated = current_user.associated
    
    respond_to do |format|
      if @referred.save

        Binnacle.create_binnacle('Crear referido', current_user, @referred.associated.full_name, 
          @referred.associated.user.email, nil, nil, 
          @referred.id, @referred.full_name, nil, nil, @referred.identification_card)

        format.html { redirect_to edit_referred_path(@referred), notice: 'Referido fue creado con éxito.' }
        format.json { render :show, status: :created, location: @referred }
      else
        format.html { render :new }
        format.json { render json: @referred.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /referreds/1
  # PATCH/PUT /referreds/1.json
  def update

    respond_to do |format|
      if @referred.update(referred_params)

        Binnacle.create_binnacle('Editar referido', current_user, @referred.associated.full_name, 
          @referred.associated.user.email, nil, nil, 
          @referred.id, @referred.full_name, nil, nil, @referred.identification_card)

        format.html { redirect_to edit_referred_path(@referred), notice: 'Referido se actualizó con éxito.' }
        format.json { render :show, status: :ok, location: @referred }
      else
        format.html { render :edit }
        format.json { render json: @referred.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /referreds/1
  # DELETE /referreds/1.json
  def destroy
    begin
      asso_name = @referred.associated.full_name
      asso_email = @referred.associated.user.email
      referred_card = @referred.identification_card
      referred_name = @referred.full_name
      referred_id = @referred.id 

      @referred.destroy
      respond_to do |format|

        Binnacle.create_binnacle('Borrar referido', current_user, asso_name, 
          asso_email, nil, nil, 
          referred_id, referred_name, nil, nil, referred_card)

        format.html { redirect_to referreds_url, notice: 'Referido fue destruido con éxito.' }
        format.json { head :no_content }
      end
    rescue ActiveRecord::InvalidForeignKey
      msg = 'El referido "'+@referred.full_name+'" tiene referimientos asignados, no puede ser eliminado.'
      respond_to do |format|
        format.html { redirect_to referreds_url, alert: msg }
      end
    end
    
  end




  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_referred
      @referred = Referred.find(params[:id])
      if @referred.associated != current_user.associated
        redirect_to referreds_url, notice: 'No puedes ver el referido seleccionado.'
      end
    end

    # Only allow a list of trusted parameters through.
    def referred_params
      params.require(:referred).permit(:names, :surnames, :identification_card, :email)
    end
end
