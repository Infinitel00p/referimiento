class AssociatedPlaceReferralsController < ApplicationController
  load_and_authorize_resource
  before_action :set_associated_place_referral, only: [:show, :edit, :update, :destroy]

  # GET /associated_place_referrals
  # GET /associated_place_referrals.json
  def index
    @associated_place_referrals = AssociatedPlaceReferral.all
  end

  # GET /associated_place_referrals/1
  # GET /associated_place_referrals/1.json
  def show
  end

  # GET /associated_place_referrals/new
  def new
    @associated_place_referral = AssociatedPlaceReferral.new
  end

  # GET /associated_place_referrals/1/edit
  def edit
  end

  # POST /associated_place_referrals
  # POST /associated_place_referrals.json
  def create
    @associated_place_referral = AssociatedPlaceReferral.new(associated_place_referral_params)

    respond_to do |format|
      if @associated_place_referral.save
        format.html { redirect_to @associated_place_referral, notice: 'Associated place referral was successfully created.' }
        format.json { render :show, status: :created, location: @associated_place_referral }
      else
        format.html { render :new }
        format.json { render json: @associated_place_referral.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /associated_place_referrals/1
  # PATCH/PUT /associated_place_referrals/1.json
  def update
    respond_to do |format|
      if @associated_place_referral.update(associated_place_referral_params)
        format.html { redirect_to @associated_place_referral, notice: 'Associated place referral was successfully updated.' }
        format.json { render :show, status: :ok, location: @associated_place_referral }
      else
        format.html { render :edit }
        format.json { render json: @associated_place_referral.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /associated_place_referrals/1
  # DELETE /associated_place_referrals/1.json
  def destroy
    @associated_place_referral.destroy
    respond_to do |format|
      format.html { redirect_to associated_place_referrals_url, notice: 'Associated place referral was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_associated_place_referral
      @associated_place_referral = AssociatedPlaceReferral.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def associated_place_referral_params
      params.require(:associated_place_referral).permit(:place_associated_id, :referral_id)
    end
end
