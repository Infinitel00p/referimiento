class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :authenticate_user!
  
  def after_sign_in_path_for(user)    
    # if user.is_admin
    #   admin_users_path
    # elsif user.is_associated
    #   referrals_path
    # elsif user.is_vigilant
    #   scan_referrals_path
    # else
    #   new_user_session_path
    # end

    root_path
     
  end

  def after_sign_out_path_for(resource_or_scope)
    new_user_session_path
  end


  rescue_from ActiveRecord::RecordNotFound, with: :handle_record_not_found
  def handle_record_not_found
    # Send it to the view that is specific for Post not found
    render_404
  end

  def render_404
    render file: "#{Rails.root}/public/404", status: :not_found
  end

  rescue_from CanCan::AccessDenied do |exception|
      
    if :authenticate_user!
      redirect_to '', alert: exception.message
    else
      redirect_to '/users/sign_in', alert: exception.message
    end

    # respond_to do |format|
    #   format.json { head :forbidden }
    #   format.html { redirect_to '/users/sign_in', :alert => exception.message }
    # end
  end
end
