class PlaceAssociatedsController < ApplicationController
  load_and_authorize_resource
  before_action :set_place_associated, only: [:show, :edit, :update, :destroy]

  # GET /place_associateds
  # GET /place_associateds.json
  def index
    @place_associateds = PlaceAssociated.all
  end

  # GET /place_associateds/1
  # GET /place_associateds/1.json
  def show
  end

  # GET /place_associateds/new
  def new
    @place_associated = PlaceAssociated.new
  end

  # GET /place_associateds/1/edit
  def edit
  end

  # POST /place_associateds
  # POST /place_associateds.json
  def create
    @place_associated = PlaceAssociated.new(place_associated_params)

    respond_to do |format|
      if @place_associated.save
        format.html { redirect_to @place_associated, notice: 'Place associated was successfully created.' }
        format.json { render :show, status: :created, location: @place_associated }
      else
        format.html { render :new }
        format.json { render json: @place_associated.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /place_associateds/1
  # PATCH/PUT /place_associateds/1.json
  def update
    respond_to do |format|
      if @place_associated.update(place_associated_params)
        format.html { redirect_to @place_associated, notice: 'Place associated was successfully updated.' }
        format.json { render :show, status: :ok, location: @place_associated }
      else
        format.html { render :edit }
        format.json { render json: @place_associated.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /place_associateds/1
  # DELETE /place_associateds/1.json
  def destroy
    @place_associated.destroy
    respond_to do |format|
      format.html { redirect_to place_associateds_url, notice: 'Place associated was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_place_associated
      @place_associated = PlaceAssociated.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def place_associated_params
      params.require(:place_associated).permit(:place_id, :associated_id)
    end
end
