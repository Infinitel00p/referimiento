class Admin::AssociatedsController < ApplicationController
  load_and_authorize_resource
  before_action :set_associated, only: [:show, :edit, :update, :destroy]

  # GET /associateds
  # GET /associateds.json
  def index
    @associateds = Associated.all
  end

  # GET /associateds/1
  # GET /associateds/1.json
  def show
  end

  # GET /associateds/new
  def new
    @associated = Associated.new
    @associated.build_user
    get_places

    @error_places = ''

  end

  # GET /associateds/1/edit
  def edit
    get_places
    
    @error_places = ''
  end

  # POST /associateds
  # POST /associateds.json
  def create
    @associated = Associated.new(associated_params)
    @associated.user.is_associated = true

    @error_places = ''
    
    @associated.valid?

    @places_selected = params[:places]
    if !@places_selected
      @error_places = 'Debe selecionar al menos 1 area'
      get_places
      render :new
      return
    end

    respond_to do |format|
      if @associated.save
        
        areas = ''
        @places_selected.each do |p_id|
          @associated.place_associateds.create(place_id:p_id)
          areas = areas + Place.find(p_id).name + ','
        end

        Binnacle.create_binnacle('Crear asociado', current_user, @associated.full_name, 
          @associated.user.email, nil, nil, nil, nil, areas)

        format.html { redirect_to admin_associateds_url(@associated), notice: 'Asociado fue creado con éxito.' }
        format.json { render :show, status: :created, location: @associated }
      else
        get_places
        format.html { render :new }
        format.json { render json: @associated.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /associateds/1
  # PATCH/PUT /associateds/1.json
  def update
    @error_places = ''
    
    @associated.valid?

    @places_selected = params[:places]
    if !@places_selected
      @error_places = 'Debe selecionar al menos 1 area'
      get_places
      render :edit
      return
    end
    


    respond_to do |format|
      if @associated.update(associated_params)
        
        areas = ''
        @places_selected.each do |p_id|
          if !@associated.place_associateds.exists?(place_id:p_id)
            @associated.place_associateds.create(place_id:p_id)
          end
          
          areas = areas + Place.find(p_id).name.to_s + ','
        end
        
        Binnacle.create_binnacle('Editar asociado', current_user, @associated.full_name, 
          @associated.user.email, nil, nil, nil, nil, areas)
        
        
        @associated.place_associateds.each do |p|  
          begin
            p.destroy if !@places_selected.include? p.place_id.to_s
          rescue
            @error_places = 'El area "'+p.place.name + '" tiene un referimiento asignado, no puede ser borrada'
            get_places
            render :edit
            return
          end
        end


        format.html { redirect_to admin_associateds_url(@associated), notice: 'Asociado se actualizó correctamente.' }
        format.json { render :show, status: :ok, location: @associated }
      else
        get_places
        format.html { render :edit }
        format.json { render json: @associated.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /associateds/1
  # DELETE /associateds/1.json
  def destroy
    begin
      asso_name = @associated.full_name
      asso_email = @associated.user.email
      @associated.destroy
      respond_to do |format|

        Binnacle.create_binnacle('Borrar asociado', current_user, asso_name, 
          asso_email, nil, nil, nil, nil, nil)
        
          format.html { redirect_to admin_associateds_url, notice: 'Asociado fue destruido con éxito.' }
        format.json { head :no_content }
      end      
    rescue ActiveRecord::InvalidForeignKey 
      msg = 'El asociado "'+@associated.identification_card.to_s+'" tiene referimientos o referidos creados, no puede ser eliminado.'
      # flash.now[:notice] 
      respond_to do |format|
        format.html { redirect_to admin_associateds_url, alert: msg }
      end
    end
    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_associated
      @associated = Associated.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def associated_params
      params.require(:associated).permit(
        :names, :surnames, :identification_card, :no_casa, :address,
        user_attributes: [:email, :password]
      )
      
    end

    def get_places
      @places = Place.all
      @places_assigned = @associated.place_associateds
    end
end
