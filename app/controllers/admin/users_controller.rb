class Admin::UsersController < ApplicationController
  load_and_authorize_resource
  before_action :set_user, only: [:show, :edit, :update]
  # before_action :set_restaurant, only: [:show, :edit, :update, :destroy]

  def index
  end

  def show
  end

  def new
    @user = User.new
    @role_error = ''
  end

  def create

    @role_id = params[:role]
    @role_error = ''

    @user = User.new(user_params)
    @user.valid?

    if @role_id.to_i == 1
      @user.is_admin = true
      role_name = 'Administrador'
    elsif @role_id.to_i == 2
      @user.is_vigilant = true
      role_name = 'Vigilante'
    else
      @role_error = 'Debe seleccionar un rol válido'
      render :new
      return
    end
      
    respond_to do |format|
      if @user.save

        Binnacle.create_binnacle('Crear usuario', current_user, nil, 
          nil, nil, nil, 
          nil, nil, nil, @user.email + ' ' + role_name, nil)
        format.html { redirect_to admin_users_path, notice: 'Usuario se creó correctamente.' }
      else
        format.html { render :new }
      end
    end
  end


  def edit
    set_role_id
    @role_error = ''

    if @user.is_associated
      redirect_to edit_admin_associated_path(@user.associated)
    end
  end

  def update
    if params[:user][:password].blank?
      params[:user].delete(:password)
    else
      @user.password_confirmation = params[:password]
    end
    
    @user.valid?

    @role_id = params[:role]
    @role_error = ''

    if @role_id.to_i == 1
      @user.is_admin = true
      @user.is_vigilant = false
      role_name = 'Administrador'
    elsif @role_id.to_i == 2
      @user.is_vigilant = true
      @user.is_admin = false
      role_name = 'Vigilante'
    else
      @role_error = 'Debe seleccionar un rol válido'
      render :edit
      return
    end

    if @user.update(user_params)
      
      Binnacle.create_binnacle('Editar usuario', current_user, nil, 
        nil, nil, nil, 
        nil, nil, nil, @user.email + ' ' +role_name, nil)

      redirect_to edit_admin_user_path, notice: 'Usuario se actualizó correctamente.'
    else
      render :edit
    end
  end

  def destroy
    begin
      email = @user.email
      @user.destroy
      respond_to do |format|

        Binnacle.create_binnacle('Borrar usuario', current_user, nil, 
          nil, nil, nil, 
          nil, nil, nil, email, nil)

        format.html { redirect_to admin_users_url, notice: 'Usuario fue destruido con éxito.' }
        format.json { head :no_content }
      end
    rescue ActiveRecord::InvalidForeignKey
      if @user.is_associated
        msg = 'El asociado "'+@user.email+'" tiene referidos o referimientos creados, no puede ser eliminado.'
      elsif @user.is_vigilant
        msg = 'El vigilante "'+@user.email+'" tiene referimientos que han sido comentados, no puede ser eliminado.'
      else
        msg = 'El usuario "'+@user.email+'" tiene referimientos que han sido comentados, no puede ser eliminado.'
      end
      respond_to do |format|
        format.html { redirect_to admin_users_url, alert: msg }
        format.json { head :no_content }
      end
    end

  end 
  private
    def set_user
      @user = User.find_by(id: params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(
        :email, :password
      )
    end

    def set_role_id 
      
      if @user.is_admin
        @role_id = '1'
      elsif @user.is_vigilant
        @role_id = '2'
      else @user.is_associated
        @role_id = '3'
      end
    end
end
