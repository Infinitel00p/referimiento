class Admin::PlacesController < ApplicationController
  load_and_authorize_resource
  before_action :set_place, only: [:show, :edit, :update, :destroy]

  # GET /places
  # GET /places.json
  def index
    @places = Place.all
  end

  # GET /places/1
  # GET /places/1.json
  def show
  end

  # GET /places/new
  def new
    @place = Place.new
  end

  # GET /places/1/edit
  def edit
  end

  # POST /places
  # POST /places.json
  def create
    @place = Place.new(place_params)

    respond_to do |format|
      if @place.save

        Binnacle.create_binnacle('Crear área', current_user, nil, 
          nil, nil, nil, 
          nil, nil, @place.name, nil, nil)
        format.html { redirect_to edit_admin_place_url(@place), notice: 'El área se creó correctamente.' }
        format.json { render :show, status: :created, location: @place }
      else
        format.html { render :new }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /places/1
  # PATCH/PUT /places/1.json
  def update
    respond_to do |format|
      if @place.update(place_params)

        Binnacle.create_binnacle('Editar área', current_user, nil, 
          nil, nil, nil, 
          nil, nil, @place.name, nil, nil)
        format.html { redirect_to edit_admin_place_url(@place), notice: 'El área se actualizó correctamente.' }
        format.json { render :show, status: :ok, location: @place }
      else
        format.html { render :edit }
        format.json { render json: @place.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /places/1
  # DELETE /places/1.json
  def destroy
    begin
      name = @place.name
      @place.destroy
      respond_to do |format|

        Binnacle.create_binnacle('Borrar área', current_user, nil, 
          nil, nil, nil, 
          nil, nil, name, nil, nil)

        format.html { redirect_to admin_places_url, notice: 'El área fue destruido con éxito.' }
        format.json { head :no_content }
      end
    rescue ActiveRecord::InvalidForeignKey
      msg = 'El area "'+@place.name+'" esta asignada a un asociado o referimiento, no puede ser eliminado.'
      respond_to do |format|
        format.html { redirect_to admin_places_url, alert: msg }
        format.json { head :no_content }
      end
    end
      
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_place
      @place = Place.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def place_params
      params.require(:place).permit(:name)
    end
end
