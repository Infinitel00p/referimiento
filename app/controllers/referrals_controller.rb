require 'rqrcode'
require "time"

class ReferralsController < ApplicationController
  load_and_authorize_resource
  before_action :set_referral, only: [:show, :edit, :update, :destroy]

  # GET /referrals
  # GET /referrals.json
  def index
    @referrals = Referral.get_by_associated current_user.associated
  end

  # GET /referrals/1
  # GET /referrals/1.json
  def show
  end

  # GET /referrals/new
  def new
    @referral = Referral.new

    @error_places = ''
  end

  # GET /referrals/1/edit
  def edit

    @error_places = ''
  end

  # POST /referrals
  # POST /referrals.json
  def create
    @referral = Referral.new(referral_params)
    @referral.associated = current_user.associated

    @error_places = ''
    
    valid = @referral.valid?
    
    @places_selected = params[:places]
    if !@places_selected
      @error_places = 'Debe selecionar al menos 1 area'
    
      if valid
        render :new
        return
      end
    end

    
    
    respond_to do |format|
      if @referral.save
        
        areas = ''
        @places_selected.each do |p_id|
          @referral.associated_place_referrals.create(place_associated_id:p_id)
          areas = areas + PlaceAssociated.find(p_id).place.name + ','
        end

        QrCodeMailer.qrcode(@referral, generate_qr(@referral.referred.id)).deliver

        Binnacle.create_binnacle('Crear referimiento', current_user, @referral.associated.full_name, 
          @referral.associated.user.email, @referral.id, @referral.date, 
          @referral.referred.id, @referral.referred.full_name, areas, nil,
          @referral.referred.identification_card)
        
        format.html { redirect_to edit_referral_url(@referral), notice: 'El referimiento se creó correctamente.' }
        format.json { render :show, status: :created, location: @referral }
      else
        format.html { render :new }
        format.json { render json: @referral.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /referrals/1
  # PATCH/PUT /referrals/1.json
  def update    
    @error_places = ''


    if @referral.date < Date.today 
      redirect_to referrals_url, alert: "Este referimiento ya caduco, crea uno nuevo." 
      return 
    end

    if @referral.used 
      redirect_to referrals_url, alert: "Este referimiento ya fue leído, no puede ser modificado." 
      return 
    end

    if @referral.comments.length > 0 
      redirect_to referrals_url, alert: "Este referimiento ya fue leído, no puede ser modificado." 
      return 
    end
    
    @places_selected = params[:places]
    if !@places_selected
      @error_places = 'Debe selecionar al menos 1 area'    
    end


    respond_to do |format|
      if @referral.update(referral_params)

        if @error_places != ''
          render :edit
          return
        end
        
        areas = ''
        @places_selected.each do |p_id|
          if !@referral.associated_place_referrals.exists?(place_associated_id:p_id) 
            @referral.associated_place_referrals.create(place_associated_id:p_id)
          end

          areas = areas + PlaceAssociated.find(p_id).place.name + ','
        end

        @referral.associated_place_referrals.each do |p|
          p.destroy if !@places_selected.include? p.place_associated_id.to_s
        end

        QrCodeMailer.qrcode(@referral, generate_qr(@referral.referred.id)).deliver

        Binnacle.create_binnacle('Editar referimiento', current_user, @referral.associated.full_name, 
          @referral.associated.user.email, @referral.id, @referral.date, 
          @referral.referred.id, @referral.referred.full_name, areas, nil,
          @referral.referred.identification_card)

        format.html { redirect_to edit_referral_url(@referral), notice: 'El referimiento se actualizó correctamente.' }
        format.json { render :show, status: :ok, location: @referral }
      else
        format.html { render :edit }
        format.json { render json: @referral.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /referrals/1
  # DELETE /referrals/1.json
  def destroy
    begin
      asso_name = @referral.associated.full_name
      asso_email = @referral.associated.user.email
      referal_id = @referral.id
      referal_date = @referral.date
      refered_name = @referral.referred.full_name  
      refered_id = @referral.referred.id
      refered_card = @referral.referred.identification_card
      
      areas = ''
      @referral.associated_place_referrals.each do |p|
        areas = areas + PlaceAssociated.find(p.place_associated_id).place.name + ','
      end
      
      @referral.destroy
      respond_to do |format|


        Binnacle.create_binnacle('Borrar referimiento', current_user, asso_name, 
          asso_email, referal_id, referal_date, refered_id, refered_name, areas, nil, 
          refered_card)
        
        format.html { redirect_to referrals_url, notice: 'El referimiento fue destruido con éxito.' }
        format.json { head :no_content }
      end
    rescue ActiveRecord::InvalidForeignKey
      msg = 'El referimiento ha sido leido, no puede ser eliminado.'
      respond_to do |format|
        format.html { redirect_to referrals_url, alert: msg }
      end
    end
  end


  def scan_qr
  end

  def add_comment
    @comment = Comment.new(referral_id: params[:referral_id], user: current_user, content: params[:content])

    @referred = Referred.find(params[:id_referred]) 
    @referrals_active = Referral.get_referrals_active @referred 
    @comments = Comment.get_by_referral @referrals_active.first  

    if @comment.valid?
      @comment.save

      areas = ''
      
      @comment.referral.associated_place_referrals.each do |p|
        areas = areas + PlaceAssociated.find(p.place_associated_id).place.name + ','
      end

      Binnacle.create_binnacle('Comentario', current_user, @referred.associated.full_name, 
        @referred.associated.user.email, @comment.referral.id, @comment.referral.date,  
        @referred.id, @referred.full_name, areas, @comment.content,
        @referred.identification_card)

      redirect_to  referrals_scanned_path+'?comment=true', notice: 'El comentario se creó correctamente.'
    else
      render :referrals_active
    end


    
  end

  def referrals_active
    @comment = Comment.new

    @referred = Referred.find(params[:id_referred]) 
    @referrals_active = Referral.get_referrals_active @referred 
    @comments = Comment.get_by_referral @referrals_active.first 
    
    if @referrals_active.length > 0
      
      @referrals_active[0].used = true
      @referrals_active[0].save
      if params[:comment] == nil
        puts 2222222222222222222222222222222222222222222222222222222222222222222222
        areas = ''
        @referrals_active[0].associated_place_referrals.each do |p|
          areas = areas + PlaceAssociated.find(p.place_associated_id).place.name + ','
        end

        Binnacle.create_binnacle('Consultar referimiento', current_user, @referrals_active[0].associated.full_name, 
          @referrals_active[0].associated.user.email, @referrals_active[0].id, @referrals_active[0].date, 
          @referrals_active[0].referred.id, @referrals_active[0].referred.full_name, areas, nil, 
          @referrals_active[0].referred.identification_card )
      end 

    else
      if @referred
        Binnacle.create_binnacle('Referimiento inválido', current_user, @referred.associated.full_name, 
          @referred.associated.user.email, nil, nil, 
          @referred.id, @referred.full_name, nil, nil,
          @referred.identification_card)
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_referral
      @referral = Referral.find(params[:id])
      if @referral.associated != current_user.associated
        redirect_to referrals_url, notice: 'No puedes ver el referimiento seleccionado.'
      end
    end

    # Only allow a list of trusted parameters through.
    def referral_params
      params.require(:referral).permit(:date, :token, :used, :referred_id)
    end


    def generate_qr(id)
      qrcode = RQRCode::QRCode.new(referrals_scanned_path(id), :size => 4, :level => :h)
      return qrcode
    end
end

