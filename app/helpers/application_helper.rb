module ApplicationHelper

    def is_active?(path)
        return 'active' if current_page?(path) || request.path.include?(path)
    end
end
