class QrCodeMailer < ApplicationMailer

    def qrcode(referral, qrcode)
        @referral = referral
        @qr_svg = qrcode
        mail(to: @referral.referred.email, subject: 'Invitación')
    end
end
