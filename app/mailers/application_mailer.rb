class ApplicationMailer < ActionMailer::Base
  default from: 'franc.jadev@gmail.com'
  layout 'mailer'
end
