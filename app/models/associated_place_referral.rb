class AssociatedPlaceReferral < ApplicationRecord
  belongs_to :place_associated
  belongs_to :referral
  validates_associated :place_associated
  validates_associated :referral

end
