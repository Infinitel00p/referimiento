

class Referred < ApplicationRecord
    belongs_to :associated
    has_many :referrals

    validates :identification_card, presence: true, uniqueness: true, numericality: { only_integer: true }
    validates :associated, presence: true
    validates :names, presence: true, length: { maximum: 40 }
    validates :surnames, presence: true, length: { maximum: 40 }
    validates :email, presence: true, format: { with: /(\A([a-z]*\s*)*\<*([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\>*\Z)/i }

    def full_name
        return names + ' ' + surnames
    end

    scope :get_by_associated, ->(associated){where(associated: associated)}


    def save
    
        if self.id.blank?
            require 'securerandom'
            self.id = SecureRandom.urlsafe_base64(11)
        end
    
        super
    end


end
