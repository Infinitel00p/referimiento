class Associated < ApplicationRecord
  belongs_to :user, dependent: :destroy
  has_many :place_associateds, dependent: :destroy
  accepts_nested_attributes_for :user

  validates :user, presence: true
  validates :names, presence: true, length: { maximum: 40 }
  validates :surnames, presence: true, length: { maximum: 40 }
  validates :no_casa, presence: true, numericality: { only_integer: true }
  validates :identification_card, presence: true, uniqueness: true, numericality: { only_integer: true }
  validates :address, presence: true, length: { maximum: 100 }


  def full_name
    return names + ' ' + surnames
  end

end
