class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :referral

  validates :content, presence: true, length: { maximum: 500 }

  scope :get_by_referral, ->(referral){where(referral: referral).order(created_at: :desc) }
end
