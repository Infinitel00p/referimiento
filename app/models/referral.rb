class Referral < ApplicationRecord
    belongs_to :associated
    belongs_to :referred
    has_many :associated_place_referrals, dependent: :destroy
    has_many :comments

    has_secure_token :token

    validates :associated, :referred, presence: true
    validates :token, presence: true, uniqueness: true, :allow_blank => true
    validates :date, presence: true

    validate :times?
    def times?
  
      if !date.blank?
        if date < Date.today
          errors.add(:date, 'debe ser mayor o igual a la de hoy')
        end
      end
  
    end

    validate :dates_following, on: :update
    def dates_following

      if referred
        if Referral.get_following_referrals(referred, id).exists?(date: date)
          errors.add(:date, 'ya tiene un referimiento para ese día')
        end
      end
      
    end


    validate :dates_act_following, on: :create
    def dates_act_following

      if referred
        if Referral.get_following_act_referrals(referred).exists?(date: date)
          errors.add(:date, 'ya tiene un referimiento para ese día')
        end
      end
      
    end

    scope :get_by_associated, ->(associated){where(associated: associated).where('date >= ?', Date.today).order(date: :asc)}

    scope :get_referrals_active, ->(referred){where(referred: referred).where('date = ?', Date.today).order(date: :asc)}
    scope :get_referrals_future, ->(referred){where(referred: referred).where('date > ?', Date.today).order(date: :asc)}

    scope :get_following_referrals, -> (referred, id){where(referred: referred).where('date >= ?', Date.today).where.not(id: id)}
    scope :get_following_act_referrals, -> (referred){where(referred: referred).where('date >= ?', Date.today)}
end
