class PlaceAssociated < ApplicationRecord
  belongs_to :place
  belongs_to :associated

  validates :associated, uniqueness: { scope: :place }
end
