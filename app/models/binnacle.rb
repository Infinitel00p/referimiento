require 'date'

class Binnacle < ApplicationRecord
  belongs_to :user


  validates :comment, length: { maximum: 500 }
      
  def Binnacle.create_binnacle(action, current_user, associated_name, associated_email, \
    referral_id, referral_date, referred_id, referred_name, areas, comment=nil, referred_card=nil)
    
    binnacle = Binnacle.new
    binnacle.date = DateTime.now()
    binnacle.action = action
    binnacle.user = current_user
    binnacle.associated_name = associated_name
    binnacle.associated_email = associated_email
    binnacle.referral_id = referral_id
    binnacle.referral_date = referral_date
    binnacle.referred_id = referred_id
    binnacle.referred_card = referred_card
    binnacle.referred_name = referred_name
    binnacle.areas = areas
    binnacle.comment = comment
    binnacle.save
  end
end
