Rails.application.routes.draw do
  
  get '', to: 'home#home', as: 'root'

  get 'unauthorized', to: 'home#unautorized'

  # mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    passwords: 'users/passwords',
    # registrations: 'users/registrations'
  }

  # namespace :user do
  #   root :to => "users#index"
  # end
  
  namespace :admin do
    # get 'users', to: 'users#index'
    # get 'users/new', to: 'users#new'
    # post 'users', to: 'users#create'
    # get 'users/:id', to: 'users#show', as: 'users_detail'
    resources :users
    resources :places
    resources :associateds

    get 'binnacle/list'
  end

  # resources :associated_place_referrals
  # resources :place_associateds
  resources :referrals
  resources :referreds
  # resources :associateds

  get ':id_referred/referrals', to: 'referrals#referrals_active', as: 'referrals_scanned'
  post ':id_referred/referrals', to: 'referrals#add_comment', as: 'referral_add_comment'
  # post 'add_comment', to: 'referrals#add_comment', as: 'referral_add_comment'
  get 'scan_qr', to: 'referrals#scan_qr', as: 'scan_referrals'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
