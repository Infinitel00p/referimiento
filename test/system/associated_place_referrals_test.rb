require "application_system_test_case"

class AssociatedPlaceReferralsTest < ApplicationSystemTestCase
  setup do
    @associated_place_referral = associated_place_referrals(:one)
  end

  test "visiting the index" do
    visit associated_place_referrals_url
    assert_selector "h1", text: "Associated Place Referrals"
  end

  test "creating a Associated place referral" do
    visit associated_place_referrals_url
    click_on "New Associated Place Referral"

    fill_in "Place associated", with: @associated_place_referral.place_associated_id
    fill_in "Referral", with: @associated_place_referral.referral_id
    click_on "Create Associated place referral"

    assert_text "Associated place referral was successfully created"
    click_on "Back"
  end

  test "updating a Associated place referral" do
    visit associated_place_referrals_url
    click_on "Edit", match: :first

    fill_in "Place associated", with: @associated_place_referral.place_associated_id
    fill_in "Referral", with: @associated_place_referral.referral_id
    click_on "Update Associated place referral"

    assert_text "Associated place referral was successfully updated"
    click_on "Back"
  end

  test "destroying a Associated place referral" do
    visit associated_place_referrals_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Associated place referral was successfully destroyed"
  end
end
