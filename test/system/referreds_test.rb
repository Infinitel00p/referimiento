require "application_system_test_case"

class ReferredsTest < ApplicationSystemTestCase
  setup do
    @referred = referreds(:one)
  end

  test "visiting the index" do
    visit referreds_url
    assert_selector "h1", text: "Referreds"
  end

  test "creating a Referred" do
    visit referreds_url
    click_on "New Referred"

    fill_in "Email", with: @referred.email
    fill_in "Id", with: @referred.id
    fill_in "Names", with: @referred.names
    fill_in "Surnames", with: @referred.surnames
    click_on "Create Referred"

    assert_text "Referred was successfully created"
    click_on "Back"
  end

  test "updating a Referred" do
    visit referreds_url
    click_on "Edit", match: :first

    fill_in "Email", with: @referred.email
    fill_in "Id", with: @referred.id
    fill_in "Names", with: @referred.names
    fill_in "Surnames", with: @referred.surnames
    click_on "Update Referred"

    assert_text "Referred was successfully updated"
    click_on "Back"
  end

  test "destroying a Referred" do
    visit referreds_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Referred was successfully destroyed"
  end
end
