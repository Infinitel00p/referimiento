require "application_system_test_case"

class PlaceAssociatedsTest < ApplicationSystemTestCase
  setup do
    @place_associated = place_associateds(:one)
  end

  test "visiting the index" do
    visit place_associateds_url
    assert_selector "h1", text: "Place Associateds"
  end

  test "creating a Place associated" do
    visit place_associateds_url
    click_on "New Place Associated"

    fill_in "Associated", with: @place_associated.associated_id
    fill_in "Place", with: @place_associated.place_id
    click_on "Create Place associated"

    assert_text "Place associated was successfully created"
    click_on "Back"
  end

  test "updating a Place associated" do
    visit place_associateds_url
    click_on "Edit", match: :first

    fill_in "Associated", with: @place_associated.associated_id
    fill_in "Place", with: @place_associated.place_id
    click_on "Update Place associated"

    assert_text "Place associated was successfully updated"
    click_on "Back"
  end

  test "destroying a Place associated" do
    visit place_associateds_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Place associated was successfully destroyed"
  end
end
