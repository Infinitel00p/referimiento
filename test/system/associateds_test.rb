require "application_system_test_case"

class AssociatedsTest < ApplicationSystemTestCase
  setup do
    @associated = associateds(:one)
  end

  test "visiting the index" do
    visit associateds_url
    assert_selector "h1", text: "Associateds"
  end

  test "creating a Associated" do
    visit associateds_url
    click_on "New Associated"

    fill_in "Address", with: @associated.address
    fill_in "Id", with: @associated.id
    fill_in "Names", with: @associated.names
    fill_in "No casa", with: @associated.no_casa
    fill_in "Surnames", with: @associated.surnames
    fill_in "User", with: @associated.user_id
    click_on "Create Associated"

    assert_text "Associated was successfully created"
    click_on "Back"
  end

  test "updating a Associated" do
    visit associateds_url
    click_on "Edit", match: :first

    fill_in "Address", with: @associated.address
    fill_in "Id", with: @associated.id
    fill_in "Names", with: @associated.names
    fill_in "No casa", with: @associated.no_casa
    fill_in "Surnames", with: @associated.surnames
    fill_in "User", with: @associated.user_id
    click_on "Update Associated"

    assert_text "Associated was successfully updated"
    click_on "Back"
  end

  test "destroying a Associated" do
    visit associateds_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Associated was successfully destroyed"
  end
end
