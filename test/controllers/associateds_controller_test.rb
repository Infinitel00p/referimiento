require 'test_helper'

class AssociatedsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @associated = associateds(:one)
  end

  test "should get index" do
    get associateds_url
    assert_response :success
  end

  test "should get new" do
    get new_associated_url
    assert_response :success
  end

  test "should create associated" do
    assert_difference('Associated.count') do
      post associateds_url, params: { associated: { address: @associated.address, id: @associated.id, names: @associated.names, no_casa: @associated.no_casa, surnames: @associated.surnames, user_id: @associated.user_id } }
    end

    assert_redirected_to associated_url(Associated.last)
  end

  test "should show associated" do
    get associated_url(@associated)
    assert_response :success
  end

  test "should get edit" do
    get edit_associated_url(@associated)
    assert_response :success
  end

  test "should update associated" do
    patch associated_url(@associated), params: { associated: { address: @associated.address, id: @associated.id, names: @associated.names, no_casa: @associated.no_casa, surnames: @associated.surnames, user_id: @associated.user_id } }
    assert_redirected_to associated_url(@associated)
  end

  test "should destroy associated" do
    assert_difference('Associated.count', -1) do
      delete associated_url(@associated)
    end

    assert_redirected_to associateds_url
  end
end
