require 'test_helper'

class PlaceAssociatedsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @place_associated = place_associateds(:one)
  end

  test "should get index" do
    get place_associateds_url
    assert_response :success
  end

  test "should get new" do
    get new_place_associated_url
    assert_response :success
  end

  test "should create place_associated" do
    assert_difference('PlaceAssociated.count') do
      post place_associateds_url, params: { place_associated: { associated_id: @place_associated.associated_id, place_id: @place_associated.place_id } }
    end

    assert_redirected_to place_associated_url(PlaceAssociated.last)
  end

  test "should show place_associated" do
    get place_associated_url(@place_associated)
    assert_response :success
  end

  test "should get edit" do
    get edit_place_associated_url(@place_associated)
    assert_response :success
  end

  test "should update place_associated" do
    patch place_associated_url(@place_associated), params: { place_associated: { associated_id: @place_associated.associated_id, place_id: @place_associated.place_id } }
    assert_redirected_to place_associated_url(@place_associated)
  end

  test "should destroy place_associated" do
    assert_difference('PlaceAssociated.count', -1) do
      delete place_associated_url(@place_associated)
    end

    assert_redirected_to place_associateds_url
  end
end
