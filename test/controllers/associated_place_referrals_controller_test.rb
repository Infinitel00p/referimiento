require 'test_helper'

class AssociatedPlaceReferralsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @associated_place_referral = associated_place_referrals(:one)
  end

  test "should get index" do
    get associated_place_referrals_url
    assert_response :success
  end

  test "should get new" do
    get new_associated_place_referral_url
    assert_response :success
  end

  test "should create associated_place_referral" do
    assert_difference('AssociatedPlaceReferral.count') do
      post associated_place_referrals_url, params: { associated_place_referral: { place_associated_id: @associated_place_referral.place_associated_id, referral_id: @associated_place_referral.referral_id } }
    end

    assert_redirected_to associated_place_referral_url(AssociatedPlaceReferral.last)
  end

  test "should show associated_place_referral" do
    get associated_place_referral_url(@associated_place_referral)
    assert_response :success
  end

  test "should get edit" do
    get edit_associated_place_referral_url(@associated_place_referral)
    assert_response :success
  end

  test "should update associated_place_referral" do
    patch associated_place_referral_url(@associated_place_referral), params: { associated_place_referral: { place_associated_id: @associated_place_referral.place_associated_id, referral_id: @associated_place_referral.referral_id } }
    assert_redirected_to associated_place_referral_url(@associated_place_referral)
  end

  test "should destroy associated_place_referral" do
    assert_difference('AssociatedPlaceReferral.count', -1) do
      delete associated_place_referral_url(@associated_place_referral)
    end

    assert_redirected_to associated_place_referrals_url
  end
end
