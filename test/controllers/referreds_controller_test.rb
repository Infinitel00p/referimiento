require 'test_helper'

class ReferredsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @referred = referreds(:one)
  end

  test "should get index" do
    get referreds_url
    assert_response :success
  end

  test "should get new" do
    get new_referred_url
    assert_response :success
  end

  test "should create referred" do
    assert_difference('Referred.count') do
      post referreds_url, params: { referred: { email: @referred.email, id: @referred.id, names: @referred.names, surnames: @referred.surnames } }
    end

    assert_redirected_to referred_url(Referred.last)
  end

  test "should show referred" do
    get referred_url(@referred)
    assert_response :success
  end

  test "should get edit" do
    get edit_referred_url(@referred)
    assert_response :success
  end

  test "should update referred" do
    patch referred_url(@referred), params: { referred: { email: @referred.email, id: @referred.id, names: @referred.names, surnames: @referred.surnames } }
    assert_redirected_to referred_url(@referred)
  end

  test "should destroy referred" do
    assert_difference('Referred.count', -1) do
      delete referred_url(@referred)
    end

    assert_redirected_to referreds_url
  end
end
