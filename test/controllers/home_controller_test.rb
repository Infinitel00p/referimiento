require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest
  test "should get unautorized" do
    get home_unautorized_url
    assert_response :success
  end

end
