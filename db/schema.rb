# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_05_04_023532) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "associated_place_referrals", id: :serial, force: :cascade do |t|
    t.bigint "place_associated_id", null: false
    t.bigint "referral_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["place_associated_id"], name: "index_associated_place_referrals_on_place_associated_id"
    t.index ["referral_id"], name: "index_associated_place_referrals_on_referral_id"
  end

  create_table "associateds", id: :serial, force: :cascade do |t|
    t.integer "identification_card", null: false
    t.string "names", limit: 40, null: false
    t.string "surnames", limit: 40, null: false
    t.integer "no_casa", null: false
    t.text "address", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["identification_card"], name: "index_associateds_on_identification_card", unique: true
    t.index ["user_id"], name: "index_associateds_on_user_id"
  end

  create_table "binnacles", id: :serial, force: :cascade do |t|
    t.datetime "date", null: false
    t.string "action", null: false
    t.integer "user_id", null: false
    t.string "associated_name"
    t.string "associated_email"
    t.integer "referral_id"
    t.date "referral_date"
    t.string "referred_id"
    t.string "referred_name"
    t.text "areas"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "comment"
    t.integer "referred_card"
    t.index ["date"], name: "index_binnacles_on_date"
    t.index ["user_id"], name: "index_binnacles_on_user_id"
  end

  create_table "comments", id: :serial, force: :cascade do |t|
    t.text "content"
    t.integer "user_id"
    t.integer "referral_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["referral_id"], name: "index_comments_on_referral_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "place_associateds", id: :serial, force: :cascade do |t|
    t.bigint "place_id", null: false
    t.bigint "associated_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["associated_id"], name: "index_place_associateds_on_associated_id"
    t.index ["place_id"], name: "index_place_associateds_on_place_id"
  end

  create_table "places", id: :serial, force: :cascade do |t|
    t.string "name", limit: 50, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "referrals", id: :serial, force: :cascade do |t|
    t.date "date", null: false
    t.string "token", null: false
    t.boolean "used", default: false
    t.bigint "associated_id", null: false
    t.string "referred_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["associated_id"], name: "index_referrals_on_associated_id"
    t.index ["referred_id"], name: "index_referrals_on_referred_id"
  end

  create_table "referreds", id: :serial, force: :cascade do |t|
    t.integer "identification_card", null: false
    t.string "names", limit: 40, null: false
    t.string "surnames", limit: 40, null: false
    t.string "email", null: false
    t.bigint "associated_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["associated_id"], name: "index_referreds_on_associated_id"
    t.index ["identification_card"], name: "index_referreds_on_identification_card", unique: true
  end

  create_table "users", id: :serial, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "is_admin", default: false
    t.boolean "is_associated", default: false
    t.boolean "is_vigilant", default: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "associated_place_referrals", "place_associateds"
  add_foreign_key "associated_place_referrals", "referrals"
  add_foreign_key "associateds", "users"
  add_foreign_key "binnacles", "users"
  add_foreign_key "comments", "referrals"
  add_foreign_key "comments", "users"
  add_foreign_key "place_associateds", "associateds"
  add_foreign_key "place_associateds", "places"
  add_foreign_key "referrals", "associateds"
  add_foreign_key "referrals", "referreds"
  add_foreign_key "referreds", "associateds"
end
