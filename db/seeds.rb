# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create! do |u|
    u.email = 'admin@admin.com'
    u.password = '123456'
    u.is_admin = true
end


User.create! do |u|
    u.email = 'vigilant@vigilant.com'
    u.password = '123456'
    u.is_vigilant = true
end



Place.create(name:'piscina')
Place.create(name:'Estacionamiento General')
Place.create(name:'Casa Principal')
Place.create(name:'Estacionamiento I')
Place.create(name:'Estacionamiento II')
Place.create(name:'Parque')
Place.create(name:'Restaurante')
Place.create(name:'Area de golf')