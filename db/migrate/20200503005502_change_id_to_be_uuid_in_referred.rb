class ChangeIdToBeUuidInReferred < ActiveRecord::Migration[5.2]
  def change
    remove_foreign_key "referrals", "referreds"
    change_column :referrals, :referred_id, :string
    change_column :referreds, :id, :string
    add_foreign_key "referrals", "referreds"
  
  end
end
