class CreateReferrals < ActiveRecord::Migration[5.2]
  def change
    create_table :referrals do |t|
      t.date :date, null: false
      t.string :token, null: false
      t.boolean :used, default: false
      
      t.references :associated, foreign_key: true, null: false
      t.references :referred, foreign_key: true, null: false

      t.timestamps
    end
  end
end
