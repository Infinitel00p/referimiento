class CreateReferreds < ActiveRecord::Migration[5.2]
  def change
    create_table :referreds do |t|
      t.string :names, limit: 40, null: false
      t.string :surnames, limit: 40, null: false
      t.integer :identification_card, null: false
      t.string :email, null: false
      t.references :associated, foreign_key: true, null: false

      t.timestamps
    end
    add_index :referreds, :identification_card, unique: true
  end
end
