class CreatePlaceAssociateds < ActiveRecord::Migration[5.2]
  def change
    create_table :place_associateds do |t|
      t.references :place, foreign_key: true, null: false
      t.references :associated, foreign_key: true, null: false

      t.timestamps
    end
    add_index :place_associateds, [:place_id, :associated_id], unique: true
  end
end
