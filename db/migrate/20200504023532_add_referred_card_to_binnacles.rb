class AddReferredCardToBinnacles < ActiveRecord::Migration[5.2]
  def change
    add_column :binnacles, :referred_card, :integer
    change_column :binnacles, :referred_id, :string
  end
end
