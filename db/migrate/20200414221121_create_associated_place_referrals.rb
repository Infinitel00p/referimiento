class CreateAssociatedPlaceReferrals < ActiveRecord::Migration[5.2]
  def change
    create_table :associated_place_referrals do |t|
      t.references :place_associated, foreign_key: true, null: false
      t.references :referral, foreign_key: true, null: false

      t.timestamps
    end
  end
end
