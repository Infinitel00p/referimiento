class AddCommentToBinnacles < ActiveRecord::Migration[5.0]
  def change
    add_column :binnacles, :comment, :text
  end
end
