class CreateBinnacles < ActiveRecord::Migration[5.0]
  def change
    create_table :binnacles do |t|
      t.datetime :date,    null: false
      t.string :action,    null: false
      t.references :user, foreign_key: true,    null: false
      t.string :associated_name
      t.string :associated_email
      t.integer :referral_id
      t.date :referral_date
      t.integer :referred_id
      t.string :referred_name
      t.text :areas

      t.timestamps
    end
    add_index :binnacles, :date
  end
end
