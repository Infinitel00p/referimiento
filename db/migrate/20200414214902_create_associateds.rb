class CreateAssociateds < ActiveRecord::Migration[5.2]
  def change
    create_table :associateds do |t|
      t.string :names, limit: 40, null: false
      t.string :surnames, limit: 40, null: false
      t.integer :identification_card, null: false
      t.integer :no_casa, null: false
      t.text :address, null: false
      t.references :user, foreign_key: true

      t.timestamps
    end

    add_index :associateds, :identification_card, unique: true
  end
end
